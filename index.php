<!-- © David Coope -->

<!-- Start of HTML5 document -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>David Coope - Passionate About User Experience</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Adds styling for content within the header, loading main and footer sections -->
    <link rel="stylesheet" type="text/css" href="/styles/main.min.css">
</head>
<body>
<!-- Landing page and navigation -->
<header>
    <?php include("includes/skip_navigation.php"); ?>
    <!-- Introduction to the portfolio owner  -->
    <section id="introduction" role="banner">
        <h1>Hi, I'm David</h1>
        <h2>I am passionate about learning and exploring UX.</h2>
    </section>
    <?php include("includes/navigation.php"); ?>
</header>
<!-- End of landing page and navigation -->
<!-- Loading the styling and functionality to the main content -->
<div id="loading" role="status"></div>
<!-- The main content -->
<main id="content" role="main">
    <!-- Odd section (background colour of white) -->
    <div>
        <!-- About the portfolio owner -->
        <section id="about" class="row">
            <h2>About Me</h2>
            <aside class="col-3">
                <img src="images/about.jpg" alt="Photograph of portfolio owner David Coope">
            </aside>
            <section class="col-9">
                <h3>Who am I?</h3>
                <p>When I explore the web, it is common that I find accessibility and usability issues within the user experiences the websites have to offer. My goal is to deliver user experiences that are self-explanatory. I'm passionate in providing a user experience that is usable and accessible and not just how pretty it looks. A good rule of thumb is to always research the users as it will be them that will be using your websites. I enjoy exploring the various methods and techniques available in order to research and understand the users' requirements in a variety of ways.</p>
                <p>I am passionate to develop my career in UX. I started as a Mobile and UX Web Developer after achieving a 1st Class Honours degree in Web Design and Development at Edge Hill University. My studies have focused on the areas of user experience design, usability testing and data analysis to develop solutions and build websites. I have developed my usability, design, research and web coding skills through projects, industry work, volunteering and blogs. I am now applying my skills and knowledge into my career to advance in the user experience industry.</p>
            </section>
            <section>
                <h3>Key Technical Skills</h3>
                <div>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>User Experience Design</li>
                    </ul>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>User Research</li>
                    </ul>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>Usability Testing</li>
                    </ul>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>Accessibility Testing</li>
                    </ul>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>Interaction Design</li>
                    </ul>
                    <ul>
                        <li><span aria-hidden="true"></span></li>
                        <li>Visual Design</li>
                    </ul>
                </div>
            </section>
        </section>
        <!-- End of about section -->
    </div>
    <!-- End of odd section -->
    <!-- Even section (background colour of hawkes blue) -->
    <div>
        <!-- About the portfolio owner's work -->
        <section id="work" class="row work">
            <h2>Work</h2>
            <aside class="col-3">
                <img src="images/be-work-ready.jpg" alt="Screenshot of the Be Work Ready website's home page">
            </aside>
            <section class="col-8">
                <h2>Be Work Ready</h2>
                <h3>Progressive Web App</h3>
                <p>The Faculty of Health and Social Care Department at Edge Hill University wanted a responsive and progress web app that provides students access to information for supporting them in their work placements. The work placements usually include working with children and young people.</p>
                <a href="work/be-work-ready">Read case study</a>
            </section>
            <section class="col-8">
                <h2>Goggles.com</h2>
                <h3>Mobile first prototyping</h3>
                <p>Developed an interactive prototype using the user-centred design development methodology, where customers can buy an entire look from a section of products, rather than just buying products individually. The interactive prototype provides a refining feature to assist customers into narrowing down their choices. Please note, this work is a university project prototype to avoid search engines from indexing the information with their actual brand and website.</p>
                <a href="work/goggles">Read case study</a>
            </section>
            <aside class="col-3">
                <img src="images/goggles.png" alt="Interactive prototype of Goggles.com's selecting an entire look from a selection of products feature">
            </aside>
            <div class="clear"></div>
            <aside class="col-3">
                <img src="images/my-gotein.png" alt="Optimised the website performance of My Gotein">
            </aside>
            <section class="col-8">
                <h2>My Gotein</h2>
                <h3>Website optimisation</h3>
                <p>Reviewed the website performance for My Gotein and suggested improvements that web developers can use to optimise their websites. The improvements include eliminating render-blocking CSS and JavaScript, optimising images, and minifying CSS and JavaScript, which were implemented into the portfolio website using the user-centred design development methodology. Please note, this work is a university project prototype to avoid search engines from indexing the information with their actual brand and website.</p>
                <a href="work/my-gotein">Read case study</a>
            </section>
        </section>
        <!-- End of work section -->
    </div>
    <!-- End of even section -->
    <!-- Odd section (background colour of white) -->
    <div>
        <!-- Portfolio owner's contact and social media details -->
        <section id="contact" class="row">
            <h2>Contact</h2>
            <div>
                <ul>
                    <li id="email"><a href="/"><span class="container" aria-hidden="true"></span><span class="text" role="status">Ensure JavaScript is enabled.</span></a></li>
                    <li><a href="https://twitter.com/DavidCoope_" target="_blank"><span class="container" aria-hidden="true"></span><span class="text">Twitter: DavidCoope_</span></a></li>
                    <li><a href="https://www.linkedin.com/in/davidcoope" target="_blank"><span class="container" aria-hidden="true"></span><span class="text">LinkedIn</span></a></li>
                </ul>
            </div>
        </section>
        <!-- End of contact section -->
    </div>
    <!-- End of odd section -->
</main>
<!-- End of main content -->
<?php include("includes/footer.php"); ?>
</body>
</html>
<!-- End of HTML5 document -->