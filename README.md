# Portfolio README
 
If you are a developer and have suggestions that could improve the portfolio's design, functionality and/or performance of its code, submit a pull request with your recommended changes. If you want to find out what my portfolio is about, please seek information at https://www.davidcoope.co.uk/

* Click on Source to see the code for the portfolio website
* Visit https://www.davidcoope.co.uk/work/my-gotein to see the case study for My Gotein
* To see the code and/or files for the suggestions implemented into the portfolio website for My Gotein, click on Source and check out the images folder for optimised images, the scripts and styles folders for minified CSS and JavaScript code, and the .htaccess file for expiring caching for content that has been stored in the user's cache for a certain period of time, compress content for improved performance, and keeping the same TCP connection for requesting resources through HTTP/HTTPS
* Visit https://www.davidcoope.co.uk/work/goggles to see the case study for Goggles.com,
* Visit https://bitbucket.org/davidcoope/mobile-first-prototyping and click on Source to see the individual designs of the interactive prototype for mobile and desktop devices