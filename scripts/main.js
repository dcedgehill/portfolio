/**
 * © David Coope
 */

/**
 * Functionality for the content and for providing navigation smoothness when interacting with the navigation links
 */

/**
 * Simplified functionality for retrieving elements by their ID
 * @param {String} x
 * @return {Element} document.getElementById(x)
 * Usage: id("ID_GOES_HERE")
 */
function id(x) { return document.getElementById(x); }

/**
 * Simplified functionality for retrieving elements by their tag name
 * @param {String} x
 * @return {Element} document.getElementsByTagName(x)
 *
 * Usage: tag_name("TAG_NAME_GOES_HERE")
 */
function tag_name(x) { return document.getElementsByTagName(x); }

/**
 * Function executes when user clicks on the website's navigation links for an auto smooth scroll effect
 * @param {Event} event
 * @return {Boolean} false
 *
 * target: Establishes what link the user has clicked on
 * current_y_position: The position as to where the user is currently at on the page, based on their Y coordinate
 * y_target: Retrieve the target element that the user is wanting to visit on
 * y_position: Calculate the Y coordinate of the target element by adding the current Y coordinate position with the
 * target element Y coordinate position
 */
function auto_scroll_to(event) {
    var target = event.target.href;

    /**
     * If the event did not occur as a click, then that means the user is arriving on the page and needs to store the
     * URL to prevent an error from occurring with the search function
     */
    if(event.type === "load") {
        target = window.location.href;
    }

    /**
     * If the link is an internal one, check if the link contains a hash
     *
     * If the link is an external one, function ends and user goes to external website
     */
    if(target.search("https://www.davidcoope.co.uk/") !== -1) {

        /**
         * If the link does contain a hash, proceed to calculate the Y coordinate for a smooth scroll using the
         * smooth scroll polyfill
         *
         * If the link does not contain a hash, function ends and nothing happens
         */
        if(target.indexOf("#") > -1) {
            var current_y_position = window.pageYOffset;

            /**
             * The following if statement will prevent the section titles from being hidden by the fixed navigation
             *
             * If the second if statement says that the introduction is not visible and the user is not on the
             * portfolio site homepage, then no additional scrolling is needed in order to prevent the section titles
             * from being blocked by the fixed navigation
             */
            if(event.type !== "load") {
                if (current_y_position < 350 && landing_page) {
                    current_y_position = current_y_position - 51;
                }
            } else {
                current_y_position = current_y_position - 51;
            }

            // Smooth scroll polyfill
            var y_target = id(target.substring(target.indexOf("#") + 1));
            var y_position = current_y_position + y_target.getBoundingClientRect().top;
            window.scroll({top: y_position, left: 0, behavior: "smooth"});
        }
    }

    // End function, in case the user clicked on a link that is an external one
    return false;
}

/**
 * Function executes when user scrolls through the website
 *
 * If the introduction section is at least 1px visible, the navigation is not fixed at the top and is static
 * underneath the introduction section
 *
 * If the introduction section is not at least 1px visible, the navigation is not static underneath the introduction
 * section and is fixed at the top
 */
function nav_scroll() {
    var scrollable = id("scrollable"), y_position = window.pageYOffset;
    if(y_position >= transform_scroll) {
        scrollable.style.position = "fixed";
    } else {
        scrollable.style.position = "static";
    }
}

/**
 * Function executes once the web page has loaded
 *
 * Generates inner HTML to prevent spam bots from reading the full email address
 */
function view_email_address() {
    var user = "contact", domain = "davidcoope.co.uk", link = user + "@" + domain;
    id("email").innerHTML = "<a href=\"mailto:" + link + "\"><span class=\"container\" aria-hidden=\"true\"></span></span><span class=\"text\">" + link + "</span></a>";
}

/**
 * Function executes once the web page has loaded
 *
 * Adds event listeners to the following tag names inside the function:
 */
function select_tag_names() {
    /**
     * Add click events to the auto smooth scroll effect function for all links to determine how links should be dealt
     * with by navigation and internal and external links
     */
    var a = tag_name("a");
    for(var i = 0; i < a.length; i++) {
        a[i].addEventListener("click", auto_scroll_to);
        //a[i].addEventListener("focus", auto_scroll_to);
    }
}

/**
 * Function executes once the web page has loaded
 *
 * Any code that needs to be executed as soon as should be prioritised in this function
 */
function init() {
    /**
     * Styling and functionality for the main content has been loaded and can now be shown to the user
     */
    id("loading").style.display = "none";
    tag_name("main")[0].style.opacity = 1;
}

/**
 * Adds event listeners to the window for the following functions:
 *
 * If the user is on the portfolio site homepage, then some of the event listeners for that page will need added to
 * the functions
 *
 * If the user is not on the portfolio site homepage, then load the nav_scroll function once to let the website know
 * that the navigation will be fixed
 */
window.addEventListener("load", init);

// To let the website know that the navigation could be fixed if the user is not on the portfolio site homepage
var landing_page = false;
var transform_scroll = 0;
if(window.location.href === "https://www.davidcoope.co.uk/" || window.location.href.indexOf("https://www.davidcoope.co.uk/#") > -1) {
    // To let the website know that the navigation will need to be static when the introduction section is visible
    landing_page = true;
    transform_scroll = 350;
    window.addEventListener("load", view_email_address);
    window.addEventListener("load", auto_scroll_to);
    window.addEventListener("scroll", nav_scroll);
    window.addEventListener("load", select_tag_names);
} else {
    window.addEventListener("load", nav_scroll);
}