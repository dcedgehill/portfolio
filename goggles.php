<!-- © David Coope -->

<!-- Start of HTML5 document -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Goggles.com Case Study - David Coope</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Adds styling for content within the header, loading main and footer sections -->
    <link rel="stylesheet" type="text/css" href="/styles/main.min.css">
</head>
<body>
<!-- Navigation -->
<header>
    <?php include("includes/skip_navigation.php"); ?>
    <?php include("includes/navigation.php"); ?>
</header>
<!-- End of navigation -->
<!-- Loading the styling and functionality to the main content -->
<div id="loading" role="status"></div>
<!-- The main content -->
<main id="content" role="main">
    <!-- Odd section (background colour of white) -->
    <div>
        <!-- About the case study -->
        <section class="row work">
            <h1>Goggles.com Case Study</h1>
            <aside class="col-3">
                <a href="/images/goggles-requirements.png" target="_blank"><img src="/images/goggles-requirements.png" alt="Stick figures represent as part of the user research"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Research and Requirements</h2>
                <p>Research was done about the typical type of customer that visits the client’s website. The research was accomplished by asking the client about typical customer’s age, gender and whether they use smartphone, tablet or desktop devices. A typical customer on their website is aged between 25 and 44, is slightly more likely to be female than male, and just over 50% use smartphone devices, about 10% use tablet devices, and about 40% use desktop devices. Research also shows that just over 50% of sales come via desktop devices, which indicates that the typical customer prefers to browse products on smartphone devices and make their purchases via desktop devices. This provided insight that the interactive prototype should focus on being developed using a mobile first approach, where it is developed from mobile to desktop. The typical customer’s age and gender was used as part of the interactive prototype to determine what categories of clothing looks were included in when selecting an entire look from a selection of products that go together.</p>
            </section>
            <aside class="col-3">
                <a href="/images/goggles-design-and-testing.png" target="_blank"><img src="/images/goggles-design-and-testing.png" class="full" alt="Using Adobe Experience Design to link the prototype designs together"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Design and Testing</h2>
                <p>The design of the interactive prototype was developed using a mobile first approach to ensure that users do not have to change devices to be able to complete different tasks using the same features. The structure of the interactive prototype was set where users can browse through all of entire looks until they find one that they are interested in, or they can use the refine selection feature to assist them in finding the entire looks that they are after. Some of the refining options include gender, colour range, clothing size, waist and UK shoe size. For each step and interaction that was developed via the use of intractable hotspots, testing was done to ensure that each interaction leads to the correct step that the user is wanting to go on. If the user wants to see clothing looks for males that has the colour blue, then the interactive prototype would display clothing looks based on those categories. If one of the interactions leads to an incorrect step, then the relevant steps and interactions would be investigated further to identify what causes the interactions to go to the irrelevant steps.</p>
            </section>
        </section>
        <!-- End of case study section -->
    </div>
    <!-- End of odd section -->
    <!-- Even section (background colour of hawkes blue) -->
    <div>
        <!-- Case study results -->
        <section class="row work">
            <h2>Results</h2>
            <p>The developed interactive prototype was then exported from the Adobe Experience Design software package to allow anyone to try out the interactive prototype, without having to install the prototyping software package. To see the developed interactive prototype in action, click on either of the buttons below this paragraph to see the mobile and desktop versions of it.</p>
            <a href="https://xd.adobe.com/view/c7bd5a6e-e8ae-49df-9e35-f05ad9a057fe/" target="_blank">Mobile prototype</a>
            <a href="https://xd.adobe.com/view/193e5ce2-f4ce-4ec2-a634-4465b1d05e4b/" target="_blank">Desktop prototype</a>
        </section>
        <!-- End of results section -->
    </div>
    <!-- End of even section -->
</main>
<!-- End of main content -->
<?php include("includes/footer.php"); ?>
</body>
</html>
<!-- End of HTML5 document -->