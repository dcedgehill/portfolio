<!-- Copyright information -->
<footer role="contentinfo">
    <p>© <?php echo date("Y"); ?> David Coope</p>
</footer>
<!-- End of copyright information -->
<!-- Load scripts for website functionality and navigation smoothness once the required assets have been processed -->
<script async src="/scripts/main.min.js"></script>
<script async src="/scripts/smoothscroll.min.js"></script>