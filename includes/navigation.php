<!-- Navigation that is fixed when scrolling past the introduction -->
<nav id="scrollable" role="navigation" aria-label="Main Menu">
    <ul>
        <li><a href="/#about">About Me</a></li>
        <li><a href="/#work">Work</a></li>
        <li><a href="/#contact">Contact</a></li>
    </ul>
</nav>