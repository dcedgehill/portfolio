<!-- © David Coope -->

<!-- Start of HTML5 document -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Be Work Ready Case Study - David Coope</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Adds styling for content within the header, loading main and footer sections -->
    <link rel="stylesheet" type="text/css" href="/styles/main.min.css">
</head>
<body>
<!-- Navigation -->
<header>
    <?php include("includes/skip_navigation.php"); ?>
    <?php include("includes/navigation.php"); ?>
</header>
<!-- End of navigation -->
<!-- Loading the styling and functionality to the main content -->
<div id="loading" role="status"></div>
<!-- The main content -->
<main id="content" role="main">
    <!-- Odd section (background colour of white) -->
    <div>
        <!-- About the case study -->
        <section class="row work">
            <h1>Be Work Ready Case Study</h1>
            <aside class="col-3">
                <a href="/images/be-work-ready.jpg" target="_blank"><img src="/images/be-work-ready.jpg" class="full" alt="Be Work Ready's home page"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Project Details</h2>
                <p><strong>Role:</strong> Mobile and UX Web Developer</p>
                <p><strong>Client:</strong> Faculty of Health and Social Care Department at Edge Hill University</p>
                <strong>Tools Used</strong>
                <ul>
                    <li>— Adobe Fireworks</li>
                    <li>— Microsoft Forms (Survey Software)</li>
                    <li>— Atom</li>
                    <li>— Microsoft Word</li>
                </ul>
                <strong>Contribution</strong>
                <ul>
                    <li>— Desk Research</li>
                    <li>— High-Fidelity Prototypes</li>
                    <li>— User Research (Intercept Survey)</li>
                    <li>— Front- and Back-end Coding</li>
                </ul>
            </section>
            <div class="clear"></div>
            <section class="col-12">
                <h2 class="small_heading">Background</h2>
                <p>Edge Hill is an award winning university based in Lancashire and has been providing higher education for over a century. The university has won many awards, including being ranked as Gold in the Teaching Excellence Framework and university of the year (2014/2015). They have faculties in Health and Social Care, Education, and Art and Sciences, all of which encourage work placements, whether that's voluntary or sandwich placements.</p>
            </section>
            <section class="col-12">
                <h2 class="small_heading">Challenge</h2>
                <p>The Faculty of Health and Social Care Department provides information about their work placement schemes to their students, which includes information such as pro-active, professionalism, safeguarding, and securing a placement. The information that they have is contained in Microsoft Word documents and PowerPoint presentations. The problem with this is that the information is difficult to access on touch screen devices and would potentially have to download the files to their devices just to access their full functionality, such as embedded videos. To solve this, I conducted some UX work to develop a responsive and progressive web app.</p>
            </section>
            <aside class="col-3">
                <a href="/images/desk-research.jpg" target="_blank"><img src="/images/desk-research.jpg" class="full" alt="Websites that discuss about the latest trends in UX"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Desk Research</h2>
                <p>I started off by researching the latest trends in UX, more towards, mobile, that I could apply when creating the high-fidelity designs. I found trends including navigation, typography, and the minimalist design. From the desk research and experimenting, I found that the design should be around the content and not the content around the design. This mean't that if I place the navigation at the bottom and doesn't provide a stronger focus to it, then the students would be able to focus on the content more without making the navigation inaccessible.</p>
            </section>
            <div class="clear"></div>
            <aside class="col-3">
                <a href="/images/before-high-fidelity-designs.jpg" target="_blank"><img src="/images/before-high-fidelity-designs.jpg" class="full" alt="The first initial design of the Be Work Ready progressive web app"></a>
                <a href="/images/after-high-fidelity-designs.jpg" target="_blank"><img src="/images/after-high-fidelity-designs.jpg" class="full" alt="The improved design of the Be Work Ready progressive web app"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">High-Fidelity Designs</h2>
                <p>After going over the latest trends with my supervisor, I began working on the visual design of the web app. I used a mobile first approach as if students need to access any work placement information while out, they need to be able to access that information quickly. I used a peach colour scheme and contained the most relevant navigation options in a bottom tabbed navigation to provide a visual design of power. The problem with this is that when reviewing other informational sites, such as BBC News and Daily Mail, they not only placed their navigation at the top and used white as their background colour, but used a hamburger navigation style instead of a tabbed one.</p>
                <p>To make all the content also accessible via navigation, I used the hamburger, while making the background colour for both content and navigation white with a black border between them. This prevents the navigation from being the main focus until students need to access other content.</p>
            </section>
            <div class="clear"></div>
            <aside class="col-3">
                <a href="/images/prototyping.jpg" target="_blank"><img src="/images/prototyping.jpg" class="full" alt="Two prototypes and their screens, one which has the navigation pushes the content, while the other has the navigation sliding over the content"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Prototyping</h2>
                <p>When designing the web app, I came across an issue with the transition of the main content when interacting with the hamburger navigation. I was wondering whether the navigation should push the content over or whether the navigation should slide over the content. I needed to test this with current students who are on the applied health and well-being programmes. Before I could test this, I needed to bring the visual design to life. This is where InVision came into play. This was probably my favourite part of the project as I have never used InVision before and found it fun bringing my designs to life using it.</p>
                <p>I experimented and applied the features InVision has. They include hotspots, partial templates, fixed headers, and device skins to provide an experience that makes users feel that they are actually on a mobile or tablet. Once completed, I downloaded them as websites, ready for testing.</p>
            </section>
            <section class="col-12">
                <h2 class="small_heading">Intercept Surveys</h2>
                <p>Once I got the prototypes completed, I had to figure out how I would test them and how I would capture how effective they are for navigating on, especially when transitioning from the content to the navigation on mobile and tablet devices. To get an idea as to what UX method I should use, I checked out an article by Christian Rohrer on the Nielsen Norman Group website. The article stated about the different types of user research methods and their context or product use when collecting data using the methods.</p>
                <p>I created intercept surveys as I needed the students to explore the prototypes first before describing their experiences. The questions included their experiences of the different versions of the prototypes, which version did they prefer, and why they preferred one version of navigation over the other for each device type. I then arranged a date and time with one of the Faculty of Health & Social Care lecturers to conduct the intercept surveys on the students.</p>
            </section>
            <aside class="col-3">
                <a href="/images/results-and-execution.jpg" target="_blank"><img src="/images/results-and-execution.jpg" class="full" alt="The survey results of the prototypes. The overall result was students preferred the navigation to slide over the content"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Results and Execution</h2>
                <p>Just over half of the responses were positive saying that the prototypes were easy to use and were accessible. There was one experience where the writing was a bit small on the tablet versions, while the other responses were mostly "N/A". This was most likely because some of the students were using the smartphone and tablet versions of the prototypes, while others were using the desktop versions. I was a bit surprised that I only got 5 responses from the intercept surveys. Although most responses preferred the navigation to slide over the content, I contacted the lecturer to try and get more students to partake. I got a response back saying that the additional 15 students that they asked all agreed that they preferred the navigation to slide over the content as they are use to how their virtual learning environment operates.</p>
                <p>I then developed the web app using HTML, SCSS and JavaScript, as well as an API that outputs the pages content. The API was developed using an open source project called OctoberCMS. The CMS allows content to be created and updated for students to access through the web app.</p>
            </section>
        </section>
        <!-- End of case study section -->
    </div>
    <!-- End of odd section -->
</main>
<!-- End of main content -->
<?php include("includes/footer.php"); ?>
</body>
</html>
<!-- End of HTML5 document -->