<!-- © David Coope -->

<!-- Start of HTML5 document -->
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>My Gotein Case Study - David Coope</title>
    <link rel="icon" href="/favicon.ico" type="image/x-icon">
    <!-- Adds styling for content within the header, loading main and footer sections -->
    <link rel="stylesheet" type="text/css" href="/styles/main.min.css">
</head>
<body>
<!-- Navigation -->
<header>
    <?php include("includes/skip_navigation.php"); ?>
    <?php include("includes/navigation.php"); ?>
</header>
<!-- End of navigation -->
<!-- Loading the styling and functionality to the main content -->
<div id="loading" role="status"></div>
<!-- The main content -->
<main id="content" role="main">
    <!-- Odd section (background colour of white) -->
    <div>
        <!-- About the case study -->
        <section class="row work">
            <h1>My Gotein Case Study</h1>
            <aside class="col-3">
                <a href="/images/my-gotein-requirements.png" target="_blank"><img src="/images/my-gotein-requirements.png" class="full" alt="Google Chrome Network Tools"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Research and Requirements</h2>
                <p>Before suggesting the improvements, the tools for website optimisation needed to be established in order to test how optimal the client’s website was. To begin, the Google Chrome developer tools were used. The Google Chrome developer tools allowed network testing to be done, which identified how the website optimisation measures across their pages. After testing the home page, series of product page and one of the product pages, the Google Chrome developer tools showed that each of the tested pages took about 2 seconds to load 1-4 megabytes of data and that was just using a desktop and connected to Wi-Fi. Accessing the website on a GPRS network connection resulted in each page taking over 4 minutes to load that 1-4 megabytes of data. Because this same pattern happened across multiple pages, their website was not optimal, meaning users using mobile devices would struggle the most to access and interact with the website’s content.</p>
            </section>
            <aside class="col-3">
                <a href="/images/my-gotein-implementation-and-testing.png" target="_blank"><img src="/images/my-gotein-implementation-and-testing.png" class="full" alt="Tools used to optimise portfolio website"></a>
            </aside>
            <section class="col-8">
                <h2 class="small_heading">Implementation and Testing</h2>
                <p>The three identified pages were entered using the page speed tool by Google to provide suggestions as to how the website can be optimised. The page speed tool by Varvy was also used to provide further insight and triangulation for accuracy. The suggested improvements were then implemented into the portfolio website to demonstrate how effective they really were. The suggested improvements can be found in the results section of this case study. To see how optimal the portfolio website is, visit the page speed tools by Google and Varvy and enter <a href="https://www.davidcoope.co.uk">https://www.davidcoope.co.uk</a> into their tools to see the live results. Each suggestion that was implemented was tested using the page speed tools by Google and Varvy on a regular basis to determine if the implementations had met the criteria of the suggestions or not. If one of the implementations haven’t met the criteria, then suggestions and the implementations that had not met the criteria would be investigated further to determine if each suggestion was actually implemented correctly or not before moving onto the next suggestion. At the end of implementing the suggestions, the portfolio website was network tested using the Google Chrome developer tools. The Google Chrome developer tools showed that the portfolio website took under 200 milliseconds to load about 80 kilobytes of data on a desktop that is connected to Wi-Fi. Accessing the portfolio website on a GPRS network connection resulted in it taking under 20 seconds to load that 80 kilobytes of data. Between the portfolio and the client’s websites, this shows that by optimising a website, the data requests were reduced and loaded much faster, saving over 4 minutes of requesting megabytes of data to just a few seconds of requesting kilobytes of data.</p>
            </section>
        </section>
        <!-- End of case study section -->
    </div>
    <!-- End of odd section -->
    <!-- Even section (background colour of hawkes blue) -->
    <div>
        <!-- Case study results -->
        <section class="row work results">
            <h2>Results</h2>
            <h3>Eliminate render-blocking CSS and JavaScript</h3>
            <p>Render-blocking CSS and JavaScript is when CSS and JavaScript files are delaying a website from being able to apply their styles and functionality to the content in an instant. The more CSS and JavaScript files a website has, the longer it will take to apply the styles and functionality, especially if the CSS and JavaScript files are big in file size. Because of render-blocking CSS and JavaScript, the website was not even able to show its content until all of the CSS and JavaScript files have been downloaded and applied. Render-blocking CSS and JavaScript was occurring in the above-the-fold-content, which is when the CSS and JavaScript files are being loaded for content that is visible on the user’s screen. Render-blocking CSS can be eliminated by splitting the CSS styling into two files. The first CSS file would contain styling for users to be able to see the website’s header and footer. This indicates to users that the website is running, but is still loading additional resources such as images. The first CSS file would also contain a loading icon/screen to visually show users that additional resources are still being loaded. The other CSS file would contain styling for users to be able to see the website’s main content after the additional resources have been loaded. Splitting the CSS styling into two files allows the main content to be shown to the users once the website has finished loading. Then any JavaScript files can be executed after that by placing them at the bottom of the HTML documents to eliminate render-blocking JavaScript. Prioritising what content should be visible first (website’s header, footer and loading screen) will eliminate render-blocking in the above-the-fold content by the time the additional resources have been downloaded for the main content.</p>
            <h3>Optimise images</h3>
            <p>Images on the web should be small in file size. The smaller the file size, the faster they load, allowing a website to load quicker overall. This is particularly useful if users are accessing the website on a mobile device who might be using a cellular network or have restricted data plans. Images can be optimised by reducing the file sizes of them in an image editor or the PageSpeed Insights tool by Google, which can automatically compress a website’s images when supplying the tool with the website’s URL that does not have optimised images. Another way to optimise or even reduce the number of images is that any which contain approximately the same width and height dimensions as each other should just be one image instead of say nine images for example. This is called CSS image sprites, which reduces those nine resources to just one, allowing other resources to load quicker. The required CSS code can then be implemented that sets the width and height of how big the sprite is and the location of what sprite to show. CSS image sprites should only be used if there is text that relate to them and can be read out loud by assistive technologies. If there is no relating text, then CSS image sprites would have to be individual image files, where the HTML alt attribute can be used in the img element.</p>
            <h3>Enable leverage browser caching</h3>
            <p>Browser caching is when a website’s resources are locally stored on a desktop or mobile device.  Browser caching should be enabled to prevent users from having to download the website’s resources for every page that they visit. This can save many kilobytes of data and results in a faster loading time due to the website’s resources already stored on the user’s device. While browser caching allows the website’s resources to be locally stored, this can become a problem if a web developer wants to make changes to resources such as images and CSS files. The local devices will still load the resources from the cache rather than download them from the website. To prevent this from happening, create an .htaccess file and inside of that, add the file types and their time periods as to when the resources stored locally should expire using ExpiresByType. What this does is, once the time period has passed for say JPEG images for example, the local devices will ignore its cache of those files, resulting in any JPEG images that were previously cached to be downloaded from the website again. Setting time periods of when resources expire will ensure that users will not access and interact with content that will not download the latest resources for an infinite time period.</p>
            <h3>Enable keep alive</h3>
            <p>Keep alive is a method for allowing requested resources to be downloaded in the same TCP connection rather than open new TCP connections per requested resource, including resources from third party websites. Having additional TCP connections can take longer for the website to download the requested resources due to the server having to open new TCP connections for each resource that is requested to be downloaded. To download the requested resources using only one TCP connection, create an .htaccess file and inside of that, set the header connection to keep alive. What this does is it tells the server to add keep alive headers to the requested resources, resulting in one TCP connection that contains all of the requested resources and ready for download at the same instance.</p>
            <h3>Enable compression</h3>
            <p>Compression allows files to be reduced in file size, resulting in the website loading faster. Depending on the configuration the server has, file compression can be set by creating an .htaccess file and inside of that, add the file types using AddOutputFilterByType for each file type. What this does is it tells the server to compress the following file types identified in the .htaccess file, which then returns the resources at a reduced file size. Using the server’s file compression does not include minification of CSS and JavaScript code, which in this case additional tools would have to be used alongside the server’s file compression.</p>
            <h3>Reduce server response time</h3>
            <p>Server response time is how long a server takes to acknowledge that a browser is requesting resources from it. If a server’s response time is slow, then it could be down to either the website being on a hosting plan that involves sharing the server with other websites or the recommendations identified in the case study have not been considered and/or implemented into the server’s configuration and website’s files. The more traffic a website receives, the slower the server is in downloading the requested resources and if that is the case, then it becomes the question of how much money is invested into paying for a dedicated server. The alternative to that would be to review the other identified recommendations to try and use them with a shared hosting plan more efficiently.</p>
            <h3>Minify CSS and JavaScript</h3>
            <p>Minification is where CSS and JavaScript code is reduced to provide a website with faster loading times. Minification checks the current formats for each of the CSS and JavaScript and removes any white spacing and comments from the code, which then reduces the lines of code for each CSS and JavaScript file from multiple lines to just one. Minifying CSS and JavaScript code allows other resources to load quicker due to the code only containing one line of code. There are many minification tools out there that can minify CSS and JavaScript code either using online tools or offline watchers. The YUI Compressor is excellent for minification, as it can minify both CSS and JavaScript code. The YUI Compressor can also be added into integrated development environment programs, such as PhpStorm, which creates and updates the minified versions of the website’s CSS and JavaScript files used in development.</p>
        </section>
        <!-- End of results section -->
    </div>
    <!-- End of even section -->
</main>
<!-- End of main content -->
<?php include("includes/footer.php"); ?>
</body>
</html>
<!-- End of HTML5 document -->